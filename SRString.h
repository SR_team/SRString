#ifndef SRSTRING_H
#define SRSTRING_H

#include <cstdint>
#include <deque>
#include <string>
#include <string_view>

#if defined( WIN32 ) || defined( _WIN32 )
#	include <windows.h>
#endif

// GCC 10.1
struct gnu_str {
	char * pString = nullptr;
	size_t iLength = 0;
	union {
		char   string[16]{ 0 };
		size_t iAllocated;
	};
	~gnu_str() {
		if ( pString != string ) std::free( pString );
	}

	const char *	 c_str() const noexcept { return pString; }
	std::string_view str() const noexcept { return std::string_view( c_str(), iLength ); }
};

// Visual Studio 2005/2008
struct msvc_str {
	void *txt_handler;
	union {
		char  string[16]{ 0 };
		char *pString;
	};
	size_t iLength	  = 0;
	size_t iAllocated = 15;
	~msvc_str() {
		if ( iAllocated > 15 ) std::free( pString );
	}

	const char *c_str() const noexcept {
		if ( iAllocated <= 15 )
			return string;
		else
			return pString;
	}
	std::string_view str() const noexcept { return std::string_view( c_str(), iLength ); }
};

class SRString {
public:
	SRString();
	explicit SRString( char ch );
	SRString( size_t len, char ch );
	explicit SRString( const std::string &s, size_t pos = 0, size_t len = -1 );
	explicit SRString( std::string_view s, size_t pos = 0, size_t len = -1 );
	SRString( const SRString &s, size_t pos = 0, size_t len = -1 );
	SRString( const char *s, size_t len = -1 );
#if __cplusplus > 201703L or defined( _MSC_VER )
	SRString( const char8_t *s, size_t len = -1 );
#endif
	SRString( std::initializer_list<char> ilist );
	SRString( int value );
	SRString( long value );
	SRString( long long value );
	SRString( unsigned value );
	SRString( unsigned long value );
	SRString( unsigned long long value );
	SRString( float value );
	SRString( double value );
	SRString( long double value );
	SRString( const std::deque<SRString> &arr );
	SRString( const gnu_str &&gnu );
	SRString( const msvc_str &&msvc );
	explicit SRString( const std::string &&str );
	SRString( const SRString &&str );
	virtual ~SRString();

	SRString &					assign( char ch ) noexcept;
	SRString &					assign( size_t len, char ch ) noexcept;
	SRString &					assign( const std::string &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &					assign( std::string_view s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &					assign( const SRString &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &					assign( const char *s, size_t len = -1 ) noexcept;
	SRString &					assign( std::initializer_list<char> ilist ) noexcept;
	template<class T> SRString &assign( const T &t ) noexcept { return assign( std::to_string( t ) ); }
	SRString &					assign( const gnu_str &&gnu ) noexcept;
	SRString &					assign( const msvc_str &&msvc ) noexcept;
	SRString &					assign( const std::string &&str ) noexcept;
	SRString &					assign( const SRString &&str ) noexcept;
	SRString &					operator=( char ch ) noexcept;
	SRString &					operator=( const std::string &s ) noexcept;
	SRString &					operator=( std::string_view s ) noexcept;
	SRString &					operator=( const SRString &s ) noexcept;
	SRString &					operator=( const char *s ) noexcept;
	SRString &					operator=( std::initializer_list<char> ilist ) noexcept;
	template<class T> SRString &operator=( const T &t ) noexcept { return assign( t ); }
	SRString &					operator=( const gnu_str &&gnu ) noexcept;
	SRString &					operator=( const msvc_str &&msvc ) noexcept;
	SRString &					operator=( const std::string &&str ) noexcept;
	SRString &					operator=( const SRString &&str ) noexcept;

	// Element access
	char &			 at( size_t pos ) noexcept;
	const char &	 at( size_t pos ) const noexcept;
	char &			 operator[]( size_t pos ) noexcept;
	const char &	 operator[]( size_t pos ) const noexcept;
	char &			 front() noexcept;
	const char &	 front() const noexcept;
	char &			 back() noexcept;
	const char &	 back() const noexcept;
	const char *	 data() const noexcept;
	const char *	 c_str() const noexcept;
	std::string		 str() const noexcept;
	std::string_view str_v() const noexcept;
#if defined( _GLIBCXX_STRING )
	std::string str_gnu() const noexcept;
#else
	gnu_str	 str_gnu() const noexcept;
#endif
#if defined( _MSC_VER )
	std::string str_msvc() const noexcept;
#else
	msvc_str str_msvc() const noexcept;
#endif
	operator const char *() const noexcept;
	operator std::string() const noexcept;
	operator std::string_view() const noexcept;
#if defined( _MSC_VER )
	operator std::string &() const noexcept;
	operator std::string *() const noexcept;
#endif

	// Iterators
	class iterator : public std::iterator<std::input_iterator_tag, // iterator_category
										  char,					   // value_type
										  char,					   // difference_type
										  const char *,			   // pointer
										  char					   // reference
										  > {
		char *data;

	public:
		explicit iterator( char *data ) : data( data ) {}
		iterator &operator++() {
			if ( *data ) data++;
			return *this;
		}
		iterator operator++( int ) {
			iterator retval = *this;
			++( *this );
			return retval;
		}
		bool  operator==( iterator other ) const { return data == other.data; }
		bool  operator!=( iterator other ) const { return !( *this == other ); }
		char &operator*() { return *data; }
	};
	iterator begin() { return iterator( _data() ); }
	iterator end() { return iterator( _data() + length() ); }
	class const_iterator : public std::iterator<std::input_iterator_tag, // iterator_category
												char,					 // value_type
												char,					 // difference_type
												const char *,			 // pointer
												char					 // reference
												> {
		const char *data;

	public:
		explicit const_iterator( const char *data ) : data( data ) {}
		const_iterator &operator++() {
			if ( *data ) data++;
			return *this;
		}
		const_iterator operator++( int ) {
			const_iterator retval = *this;
			++( *this );
			return retval;
		}
		bool	  operator==( const_iterator other ) const { return data == other.data; }
		bool	  operator!=( const_iterator other ) const { return !( *this == other ); }
		reference operator*() const { return *data; }
	};
	const_iterator cbegin() const { return const_iterator( data() ); }
	const_iterator cend() const { return const_iterator( data() + length() ); }
	class reverse_iterator : public std::iterator<std::input_iterator_tag, // iterator_category
												  char,					   // value_type
												  char,					   // difference_type
												  const char *,			   // pointer
												  char					   // reference
												  > {
		char * data;
		size_t len;

	public:
		explicit reverse_iterator( char *data, size_t len ) : data( data ), len( len ) {}
		reverse_iterator &operator++() {
			if ( len != static_cast<size_t>( -1 ) ) len--;
			return *this;
		}
		reverse_iterator operator++( int ) {
			reverse_iterator retval = *this;
			++( *this );
			return retval;
		}
		bool  operator==( reverse_iterator other ) const { return data == other.data && len == other.len; }
		bool  operator!=( reverse_iterator other ) const { return !( *this == other ); }
		char &operator*() { return data[len]; }
	};
	reverse_iterator rbegin() { return reverse_iterator( _data(), length() - 1 ); }
	reverse_iterator rend() { return reverse_iterator( _data(), static_cast<size_t>( -1 ) ); }
	class const_reverse_iterator : public std::iterator<std::input_iterator_tag, // iterator_category
														char,					 // value_type
														char,					 // difference_type
														const char *,			 // pointer
														char					 // reference
														> {
		const char *data;
		size_t		len;

	public:
		explicit const_reverse_iterator( const char *data, size_t len ) : data( data ), len( len ) {}
		const_reverse_iterator &operator++() {
			if ( len != static_cast<size_t>( -1 ) ) len--;
			return *this;
		}
		const_reverse_iterator operator++( int ) {
			const_reverse_iterator retval = *this;
			++( *this );
			return retval;
		}
		bool	  operator==( const_reverse_iterator other ) const { return data == other.data && len == other.len; }
		bool	  operator!=( const_reverse_iterator other ) const { return !( *this == other ); }
		reference operator*() const { return data[len]; }
	};
	const_reverse_iterator crbegin() const { return const_reverse_iterator( data(), length() - 1 ); }
	const_reverse_iterator crend() const { return const_reverse_iterator( data(), static_cast<size_t>( -1 ) ); }

	// Capacity
	bool   empty() const noexcept;
	size_t size() const noexcept;
	size_t length() const noexcept;
	size_t max_size() const noexcept;
	bool   reserve( size_t new_cap = 0 ) noexcept;
	size_t capacity() const noexcept;
	void   shrink_to_fit() noexcept;

	// Operations
	void	  clear() noexcept;
	SRString &insert( size_t index, size_t count, char ch ) noexcept;
	SRString &insert( size_t index, const char *s, size_t len = -1 ) noexcept;
	SRString &insert( size_t index, const std::string &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &insert( size_t index, const std::string_view &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &insert( size_t index, const SRString &s, size_t pos = 0, size_t len = -1 ) noexcept;
	template<class T> SRString &insert( size_t index, const T &t ) noexcept {
		return insert( index, std::to_string( t ) );
	}
	SRString &insert( iterator it, size_t count, char ch ) noexcept;
	SRString &insert( iterator it, const char *s, size_t len = -1 ) noexcept;
	SRString &insert( iterator it, const std::string &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &insert( iterator it, const std::string_view &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &insert( iterator it, const SRString &s, size_t pos = 0, size_t len = -1 ) noexcept;
	template<class T> SRString &insert( iterator it, const T &t ) noexcept { return insert( it, std::to_string( t ) ); }
	SRString &					erase( size_t index = 0, size_t len = -1 ) noexcept;
	SRString &					erase( iterator it, size_t len = -1 ) noexcept;
	SRString &					erase( iterator from, iterator to ) noexcept;
	SRString &					erase( size_t index, iterator to ) noexcept;
	bool						push_back( char ch ) noexcept;
	bool						pop_back() noexcept;
	SRString &					append( char ch ) noexcept;
	SRString &					append( size_t len, const char ch ) noexcept;
	SRString &					append( const std::string &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &					append( std::string_view s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &					append( const SRString &s, size_t pos = 0, size_t len = -1 ) noexcept;
	SRString &					append( const char *s, size_t len = -1 ) noexcept;
	SRString &					append( std::initializer_list<char> ilist ) noexcept;
	//	template<class T> SRString &append( const T &t ) noexcept { return append( std::to_string( t ) ); }
	SRString &operator+=( char ch ) noexcept;
	SRString &operator+=( const std::string &s ) noexcept;
	SRString &operator+=( std::string_view s ) noexcept;
	SRString &operator+=( const SRString &s ) noexcept;
	SRString &operator+=( const char *s ) noexcept;
	SRString &operator+=( std::initializer_list<char> ilist ) noexcept;
	//	template<class T> SRString &operator+=( const T &t ) noexcept { return append( t ); }
	int					  compare( const SRString &str ) const noexcept;
	int					  compare( const std::string &str ) const noexcept;
	int					  compare( std::string_view str ) const noexcept;
	int					  compare( const char *str ) const noexcept;
	int					  compare( char ch ) const noexcept;
	template<class T> int compare( const T &t ) const noexcept { return compare( std::to_string( t ) ); }
	bool				  starts_with( const SRString &s ) const noexcept;
	bool				  starts_with( const std::string &s ) const noexcept;
	bool				  starts_with( std::string_view s ) const noexcept;
	bool				  starts_with( const char *s ) const noexcept;
	bool				  starts_with( char ch ) const noexcept;
	template<class T> int starts_with( const T &t ) const noexcept { return starts_with( std::to_string( t ) ); }
	bool				  ends_with( const SRString &s ) const noexcept;
	bool				  ends_with( const std::string &s ) const noexcept;
	bool				  ends_with( std::string_view s ) const noexcept;
	bool				  ends_with( const char *s ) const noexcept;
	bool				  ends_with( char ch ) const noexcept;
	template<class T> int ends_with( const T &t ) const noexcept { return ends_with( std::to_string( t ) ); }
	SRString &			  replace( size_t pos, size_t count, std::string_view str ) noexcept;
	SRString &			  replace( iterator it, size_t count, std::string_view str ) noexcept;
	SRString &			  replace( iterator from, iterator to, std::string_view str ) noexcept;
	SRString &			  replace( size_t pos, iterator to, std::string_view str ) noexcept;
	SRString &			  replace_first( std::string_view old, std::string_view str ) noexcept;
	SRString &			  replace_all( std::string_view old, std::string_view str ) noexcept;
	SRString			  substr( size_t pos = 0, size_t count = -1 ) const noexcept;
	SRString			  substr( iterator it, size_t count = -1 ) const noexcept;
	SRString			  substr( iterator from, iterator to ) const noexcept;
	size_t				  copy( char *dest, size_t count, size_t pos = 0 ) const noexcept;
	bool				  resize( size_t count, char ch = '\0' ) noexcept;
	void				  swap( SRString &other ) noexcept;
	SRString &			  to_upper();
	SRString &			  to_lower();
	SRString			  as_upper();
	SRString			  as_lower();
	std::deque<SRString>  split( char delim = ',' );
	void				  join( const std::deque<SRString> &arr ) noexcept;
	void				  operator+=( const std::deque<SRString> &arr ) noexcept;
	void				  operator=( const std::deque<SRString> &arr ) noexcept;
	template<typename... Args> SRString &args( Args... args ) {
		size_t	 size = snprintf( nullptr, 0, data(), args... ) + 1;
		SRString str;
		if ( size >= str.capacity() )
			if ( !str.reserve( size ) ) return *this;
		snprintf( (char *)str.data(), size, data(), args... );
		assign( str );
		return *this;
	}
#if defined( WIN32 ) || defined( _WIN32 )
#	if __cplusplus > 201703L or defined( _MSC_VER )
	static SRString u8_to_cp1251( const char8_t *str );
	static SRString cp1251_to_u8( const char8_t *str );
#	else
	static SRString u8_to_cp1251( const char *str );
	static SRString cp1251_to_u8( const char *str );
#	endif
	size_t	  u8_length() const noexcept;
	SRString &to_cp1251();
	SRString &to_u8();
	SRString  as_cp1251();
	SRString  as_u8();
#endif

	// Static operations
	static SRString							   as_upper( SRString str );
	static SRString							   as_upper( std::string str );
	static SRString							   as_upper( std::string_view str );
	static SRString							   as_upper( const char *str );
	static SRString							   as_lower( SRString str );
	static SRString							   as_lower( std::string str );
	static SRString							   as_lower( std::string_view str );
	static SRString							   as_lower( const char *str );
	template<typename... Args> static SRString fmt( SRString fmt, Args... args ) {
		fmt.args( args... );
		return fmt;
	}

	// Search
	size_t				  find( const SRString &str, size_t pos = 0, size_t len = -1 ) const noexcept;
	size_t				  find( const std::string &str, size_t pos = 0, size_t len = -1 ) const noexcept;
	size_t				  find( std::string_view str, size_t pos = 0, size_t len = -1 ) const noexcept;
	size_t				  find( const char *str, size_t pos = 0, size_t len = -1 ) const noexcept;
	size_t				  find( char ch, size_t pos = 0 ) const noexcept;
	template<class T> int find( const T &t, size_t pos = 0 ) const noexcept { return find( std::to_string( t ), pos ); }
	size_t				  find( const SRString &str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  find( const std::string &str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  find( std::string_view str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  find( const char *str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  find( char ch, iterator it ) const noexcept;
	template<class T> int find( const T &t, iterator it ) const noexcept { return find( std::to_string( t ), it ); }
	size_t				  find( const SRString &str, iterator from, iterator to ) const noexcept;
	size_t				  find( const std::string &str, iterator from, iterator to ) const noexcept;
	size_t				  find( std::string_view str, iterator from, iterator to ) const noexcept;
	size_t				  find( const char *str, iterator from, iterator to ) const noexcept;
	size_t				  rfind( const SRString &str, size_t pos = -1, size_t len = -1 ) const noexcept;
	size_t				  rfind( const std::string &str, size_t pos = -1, size_t len = -1 ) const noexcept;
	size_t				  rfind( std::string_view str, size_t pos = -1, size_t len = -1 ) const noexcept;
	size_t				  rfind( const char *str, size_t pos = -1, size_t len = -1 ) const noexcept;
	size_t				  rfind( char ch, size_t pos = -1 ) const noexcept;
	template<class T> int rfind( const T &t, size_t pos = -1 ) const noexcept {
		return rfind( std::to_string( t ), pos );
	}
	size_t				  rfind( const SRString &str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  rfind( const std::string &str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  rfind( std::string_view str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  rfind( const char *str, iterator it, size_t len = -1 ) const noexcept;
	size_t				  rfind( char ch, iterator it ) const noexcept;
	template<class T> int rfind( const T &t, iterator it ) const noexcept { return rfind( std::to_string( t ), it ); }

	// Constants
	static const size_t npos = static_cast<size_t>( -1 );

protected:
	union {
		char  string[16]{ 0 };
		char *pString;
	};
	size_t iLength	  = 0;
	size_t iAllocated = 15;

	char *_data() noexcept;
	bool  shift_right( size_t from, size_t count ) noexcept;
	bool  shift_left( size_t from, size_t count ) noexcept;
};

// Non-member functions
SRString operator+( const SRString &lhs, const SRString &rhs ) noexcept;
SRString operator+( const std::string &lhs, const SRString &rhs ) noexcept;
bool	 operator==( const SRString &lhs, const SRString &rhs ) noexcept;
bool	 operator==( const SRString &lhs, const std::string &rhs ) noexcept;
bool	 operator==( const SRString &lhs, std::string_view rhs ) noexcept;
bool	 operator==( const SRString &lhs, const char *rhs ) noexcept;
bool	 operator==( const SRString &lhs, char rhs ) noexcept;
bool	 operator!=( const SRString &lhs, const SRString &rhs ) noexcept;
bool	 operator!=( const SRString &lhs, const std::string &rhs ) noexcept;
bool	 operator!=( const SRString &lhs, std::string_view rhs ) noexcept;
bool	 operator!=( const SRString &lhs, const char *rhs ) noexcept;
bool	 operator!=( const SRString &lhs, char rhs ) noexcept;

// Input/output
std::basic_ostream<char> &operator<<( std::basic_ostream<char> &os, const SRString &str );
std::basic_istream<char> &operator>>( std::basic_istream<char> &is, SRString &str );

#endif // SRSTRING_H
