#include "SRString.h"
#include <algorithm>
#include <cstring>
#include <istream>
#include <limits>
#include <ostream>
#include <sstream>

SRString::SRString() {}

SRString::SRString( char ch ) {
	assign( ch );
}

SRString::SRString( size_t len, char ch ) {
	assign( len, ch );
}

SRString::SRString( const std::string &s, size_t pos, size_t len ) {
	assign( s, pos, len );
}

SRString::SRString( std::string_view s, size_t pos, size_t len ) {
	assign( s, pos, len );
}

SRString::SRString( const SRString &s, size_t pos, size_t len ) {
	assign( s, pos, len );
}

SRString::SRString( const char *s, size_t len ) {
	assign( s, len );
}

SRString::SRString( const char8_t *s, size_t len ) {
	assign( (const char *)s, len );
}

SRString::SRString( std::initializer_list<char> ilist ) {
	assign( ilist );
}

SRString::SRString( int value ) {
	assign( value );
}

SRString::SRString( long value ) {
	assign( value );
}

SRString::SRString( long long value ) {
	assign( value );
}

SRString::SRString( unsigned value ) {
	assign( value );
}

SRString::SRString( unsigned long value ) {
	assign( value );
}

SRString::SRString( unsigned long long value ) {
	assign( value );
}

SRString::SRString( float value ) {
	assign( value );
}

SRString::SRString( double value ) {
	assign( value );
}

SRString::SRString( long double value ) {
	assign( value );
}

SRString::SRString( const std::deque<SRString> &arr ) {
	join( arr );
}

SRString::SRString( const gnu_str &&gnu ) {
	assign( std::move( gnu ) );
}

SRString::SRString( const msvc_str &&msvc ) {
	assign( std::move( msvc ) );
}

SRString::SRString( const std::string &&str ) {
	assign( std::move( str ) );
}

SRString::SRString( const SRString &&str ) {
	assign( std::move( str ) );
}

SRString::~SRString() {
	if ( iAllocated > 15 ) std::free( pString );
}

SRString &SRString::assign( char ch ) noexcept {
	return assign( 1, ch );
}

SRString &SRString::assign( size_t len, char ch ) noexcept {
	if ( len > capacity() )
		if ( !reserve( len ) ) return *this;
	std::memset( _data(), ch, len );
	iLength = len;
	return *this;
}

SRString &SRString::assign( const std::string &s, size_t pos, size_t len ) noexcept {
	return assign( s.data() + pos, len );
}

SRString &SRString::assign( std::string_view s, size_t pos, size_t len ) noexcept {
	return assign( s.data() + pos, len );
}

SRString &SRString::assign( const SRString &s, size_t pos, size_t len ) noexcept {
	return assign( s.data() + pos, len );
}

SRString &SRString::assign( const char *s, size_t len ) noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = std::strlen( s );
	if ( len > capacity() )
		if ( !reserve( len ) ) return *this;
	if ( len ) std::memcpy( _data(), s, len );
	if ( _data()[len] ) _data()[len] = '\0';
	iLength = len;
	return *this;
}

SRString &SRString::assign( std::initializer_list<char> ilist ) noexcept {
	clear();
	for ( auto &&ch : ilist ) push_back( ch );
	return *this;
}

SRString &SRString::assign( const gnu_str &&gnu ) noexcept {
	if ( capacity() > 15 ) std::free( pString );
	if ( gnu.pString == gnu.string ) {
		iAllocated = 15;
		std::memcpy( string, gnu.string, gnu.iLength );
		if ( string[gnu.iLength] ) string[gnu.iLength] = '\0';
	} else {
		iAllocated = gnu.iAllocated;
		pString	   = gnu.pString;
	}
	iLength = gnu.iLength;
	return *this;
}

SRString &SRString::assign( const msvc_str &&msvc ) noexcept {
	swap( (SRString &)msvc );
	return *this;
}

SRString &SRString::assign( const std::string &&str ) noexcept {
#ifdef _GLIBCXX_STRING
	return assign( std::move( (const gnu_str &&)str ) );
#elif _MSC_VER
	return assign( std::move( (const msvc_str &&)str ) );
#else
	if ( capacity() > 15 ) std::free( pString );
	iLength = str.length();
	if ( iLength <= 15 ) {
		iAllocated = 15;
		std::memcpy( string, str.data(), str.length() );
		if ( string[str.length()] ) string[str.length()] = '\0';
	} else {
		iAllocated = str.capacity();
		pString	   = (char *)str.data();
	}
	return *this;
#endif
}

SRString &SRString::assign( const SRString &&str ) noexcept {
	swap( (SRString &)str );
	return *this;
}

SRString &SRString::operator=( char ch ) noexcept {
	return assign( ch );
}

SRString &SRString::operator=( const std::string &s ) noexcept {
	return assign( s );
}

SRString &SRString::operator=( std::string_view s ) noexcept {
	return assign( s );
}

SRString &SRString::operator=( const SRString &s ) noexcept {
	return assign( s );
}

SRString &SRString::operator=( const char *s ) noexcept {
	return assign( s );
}

SRString &SRString::operator=( std::initializer_list<char> ilist ) noexcept {
	return assign( ilist );
}

SRString &SRString::operator=( const gnu_str &&gnu ) noexcept {
	return assign( std::move( gnu ) );
}

SRString &SRString::operator=( const msvc_str &&msvc ) noexcept {
	return assign( std::move( msvc ) );
}

SRString &SRString::operator=( const std::string &&str ) noexcept {
	return assign( std::move( str ) );
}

SRString &SRString::operator=( const SRString &&str ) noexcept {
	return assign( std::move( str ) );
}

char &SRString::at( size_t pos ) noexcept {
	if ( pos < length() ) return _data()[pos];
	return _data()[length()];
}

const char &SRString::at( size_t pos ) const noexcept {
	if ( pos < length() ) return data()[pos];
	return data()[length()];
}

char &SRString::operator[]( size_t pos ) noexcept {
	return at( pos );
}

const char &SRString::operator[]( size_t pos ) const noexcept {
	return at( pos );
}

char &SRString::front() noexcept {
	return at( 0 );
}

const char &SRString::front() const noexcept {
	return at( 0 );
}

char &SRString::back() noexcept {
	if ( length() ) return at( length() - 1 );
	return at( 0 );
}

const char &SRString::back() const noexcept {
	if ( length() ) return at( length() - 1 );
	return at( 0 );
}

const char *SRString::data() const noexcept {
	if ( capacity() > 15 ) return pString;
	return string;
}

const char *SRString::c_str() const noexcept {
	return data();
}

std::string SRString::str() const noexcept {
	return std::string( data(), length() );
}

std::string_view SRString::str_v() const noexcept {
	return std::string_view( data(), length() );
}

#if defined( _GLIBCXX_STRING )
std::string SRString::str_gnu() const noexcept {
#else
gnu_str SRString::str_gnu() const noexcept {
#endif
	gnu_str gnu;
	if ( capacity() <= 15 ) {
		gnu.pString = gnu.string;
		gnu.iLength = copy( gnu.string, length() );
	} else {
		gnu.pString = (char *)std::malloc( capacity() + 1 );
		if ( gnu.pString ) {
			gnu.iLength	   = copy( gnu.pString, length() );
			gnu.iAllocated = capacity();
		}
	}
#if defined( _GLIBCXX_STRING )
	return *(std::string *)&gnu;
#else
	return gnu;
#endif
}

#if defined( _MSC_VER )
std::string SRString::str_msvc() const noexcept {
#else
msvc_str SRString::str_msvc() const noexcept {
#endif
	msvc_str msvc;
	if ( capacity() <= 15 )
		msvc.iLength = copy( msvc.string, length() );
	else {
		msvc.pString = (char *)std::malloc( capacity() + 1 );
		if ( msvc.pString ) {
			msvc.iLength	= copy( msvc.pString, length() );
			msvc.iAllocated = capacity();
		}
	}
#if defined( _MSC_VER )
	return *(std::string *)&msvc;
#else
	return msvc;
#endif
}

SRString::operator std::string() const noexcept {
#ifdef _MSC_VER
	return ( std::string )( *this );
#else
	return str();
#endif
}

SRString::operator const char *() const noexcept {
	return c_str();
}

SRString::operator std::string_view() const noexcept {
	return std::string_view( data(), length() );
}
#if defined( _MSC_VER )
SRString::operator std::string &() const noexcept {
	return (std::string &)( *this );
}
SRString::operator std::string *() const noexcept {
	return (std::string *)( this );
}
#endif

bool SRString::empty() const noexcept {
	return length() == 0;
}

size_t SRString::size() const noexcept {
	return iLength;
}

size_t SRString::length() const noexcept {
	return size();
}

#ifdef _MSC_VER
#	undef max
#endif
size_t SRString::max_size() const noexcept {
	return std::numeric_limits<size_t>::max() - 1;
}

bool SRString::reserve( size_t new_cap ) noexcept {
	if ( !new_cap ) return false;

	auto new_alloc = ( new_cap | 0xF );
	if ( ( new_cap | 0xF ) <= static_cast<size_t>( -2 ) ) {
		if ( new_alloc / 3 < ( iAllocated >> 1 ) && iAllocated <= static_cast<size_t>( -2 ) - ( iAllocated >> 1 ) )
			new_alloc = ( iAllocated >> 1 ) + iAllocated;
	} else
		new_alloc = new_cap;

	if ( new_alloc == 0 || new_alloc >= max_size() ) return false;

	if ( new_alloc < length() ) {
		if ( _data()[new_alloc] ) _data()[new_alloc] = '\0';
		iLength = new_alloc;
	}

	if ( new_alloc <= 15 && iAllocated > 15 ) {
		auto ptr = pString;
		if ( length() )
			std::memcpy( string, ptr, length() );
		else if ( string[0] )
			string[0] = '\0';
		std::free( ptr );
	} else if ( new_alloc > 15 ) {
		if ( iAllocated > 15 ) {
			auto ptr = (char *)std::realloc( pString, new_alloc + 1 );
			if ( ptr ) pString = ptr;
		} else {
			auto ptr = (char *)std::malloc( new_alloc + 1 );
			if ( ptr ) {
				if ( length() )
					std::memcpy( ptr, string, length() );
				else
					ptr[0] = '\0';
				pString = ptr;
			}
		}
	}

	iAllocated = new_alloc;
	return true;
}

size_t SRString::capacity() const noexcept {
	//	if ( iAllocated <= iLength ) iAllocated = iLength + 1;
	return iAllocated;
}

void SRString::shrink_to_fit() noexcept {
	if ( capacity() <= length() + 1 ) return;
	auto ptr = (char *)std::realloc( pString, length() + 1 );
	if ( ptr ) pString = ptr;
}

void SRString::clear() noexcept {
	if ( _data()[0] ) _data()[0] = '\0';
	iLength = 0;
}

SRString &SRString::insert( size_t index, size_t count, char ch ) noexcept {
	if ( !shift_right( index, count ) ) return *this;
	std::memset( _data() + index, ch, count );
	iLength += count;
	return *this;
}

SRString &SRString::insert( size_t index, const char *s, size_t len ) noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = std::strlen( s );
	if ( !shift_right( index, len ) ) return *this;
	std::memcpy( _data() + index, s, len );
	iLength += len;
	return *this;
}

SRString &SRString::insert( size_t index, const std::string &s, size_t pos, size_t len ) noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = s.length();
	return insert( index, s.data() + pos, len );
}

SRString &SRString::insert( size_t index, const std::string_view &s, size_t pos, size_t len ) noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = s.length();
	return insert( index, s.data() + pos, len );
}

SRString &SRString::insert( size_t index, const SRString &s, size_t pos, size_t len ) noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = s.length();
	return insert( index, s.data() + pos, len );
}

SRString &SRString::insert( SRString::iterator it, size_t count, char ch ) noexcept {
	return insert( &( *it ) - data(), count, ch );
}

SRString &SRString::insert( SRString::iterator it, const char *s, size_t len ) noexcept {
	return insert( &( *it ) - data(), s, len );
}

SRString &SRString::insert( SRString::iterator it, const std::string &s, size_t pos, size_t len ) noexcept {
	return insert( &( *it ) - data(), s, pos, len );
}

SRString &SRString::insert( SRString::iterator it, const std::string_view &s, size_t pos, size_t len ) noexcept {
	return insert( &( *it ) - data(), s, pos, len );
}

SRString &SRString::insert( SRString::iterator it, const SRString &s, size_t pos, size_t len ) noexcept {
	return insert( &( *it ) - data(), s, pos, len );
}

SRString &SRString::erase( size_t index, size_t len ) noexcept {
	if ( !index && len == static_cast<size_t>( -1 ) ) {
		clear();
		return *this;
	} else if ( len == static_cast<size_t>( -1 ) )
		len = length();
	if ( shift_left( index, len ) ) iLength = std::strlen( data() );
	return *this;
}

SRString &SRString::erase( SRString::iterator it, size_t len ) noexcept {
	return erase( &( *it ) - data(), len );
}

SRString &SRString::erase( SRString::iterator from, SRString::iterator to ) noexcept {
	if ( &( *to ) > &( *from ) ) std::swap( from, to );
	return erase( &( *from ) - data(), &( *to ) - &( *from ) );
}

SRString &SRString::erase( size_t index, SRString::iterator to ) noexcept {
	size_t end = &( *to ) - data();
	if ( index > end ) return *this;
	return erase( index, end - index );
}

bool SRString::push_back( char ch ) noexcept {
	if ( length() + 1 > capacity() )
		if ( !reserve( length() + 1 ) ) return false;
	_data()[iLength++] = ch;
	if ( _data()[iLength] ) _data()[iLength] = '\0';
	return true;
}

bool SRString::pop_back() noexcept {
	if ( !iLength ) return false;
	_data()[--iLength] = '\0';
	return true;
}

SRString &SRString::append( char ch ) noexcept {
	return append( 1, ch );
}

SRString &SRString::append( size_t len, const char ch ) noexcept {
	if ( length() + len > capacity() )
		if ( !reserve( length() + len ) ) return *this;
	std::memset( _data() + length(), ch, len );
	iLength += len;
	if ( _data()[iLength] ) _data()[iLength] = '\0';
	return *this;
}

SRString &SRString::append( const std::string &s, size_t pos, size_t len ) noexcept {
	return append( s.data() + pos, len );
}

SRString &SRString::append( std::string_view s, size_t pos, size_t len ) noexcept {
	return append( s.data() + pos, len );
}

SRString &SRString::append( const SRString &s, size_t pos, size_t len ) noexcept {
	return append( s.data() + pos, len );
}

SRString &SRString::append( const char *s, size_t len ) noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = std::strlen( s );
	if ( length() + len > capacity() )
		if ( !reserve( length() + len ) ) return *this;
	std::memcpy( _data() + length(), s, len );
	iLength += len;
	if ( _data()[iLength] ) _data()[iLength] = '\0';
	return *this;
}

SRString &SRString::append( std::initializer_list<char> ilist ) noexcept {
	for ( auto &&ch : ilist ) push_back( ch );
	return *this;
}

SRString &SRString::operator+=( char ch ) noexcept {
	return append( ch );
}

SRString &SRString::operator+=( const std::string &s ) noexcept {
	return append( s );
}

SRString &SRString::operator+=( std::string_view s ) noexcept {
	return append( s );
}

SRString &SRString::operator+=( const SRString &s ) noexcept {
	return append( s );
}

SRString &SRString::operator+=( const char *s ) noexcept {
	return append( s );
}

SRString &SRString::operator+=( std::initializer_list<char> ilist ) noexcept {
	return append( ilist );
}

int SRString::compare( const SRString &str ) const noexcept {
	return compare( str.data() );
}

int SRString::compare( const std::string &str ) const noexcept {
	return compare( str.data() );
}

int SRString::compare( std::string_view str ) const noexcept {
	return compare( str.data() );
}

int SRString::compare( const char *str ) const noexcept {
	return std::strcmp( data(), str );
}

int SRString::compare( char ch ) const noexcept {
	if ( length() == 1 )
		return std::memcmp( data(), &ch, 1 );
	else if ( length() )
		return 1;
	return -1;
}

bool SRString::starts_with( const SRString &s ) const noexcept {
	return find( s ) == 0;
}

bool SRString::starts_with( const std::string &s ) const noexcept {
	return find( s ) == 0;
}

bool SRString::starts_with( std::string_view s ) const noexcept {
	return find( s ) == 0;
}

bool SRString::starts_with( const char *s ) const noexcept {
	return find( s ) == 0;
}

bool SRString::starts_with( char ch ) const noexcept {
	return length() && front() == ch;
}

bool SRString::ends_with( const SRString &s ) const noexcept {
	return !std::memcmp( data() + ( length() - s.length() ), s.data(), s.length() );
}

bool SRString::ends_with( const std::string &s ) const noexcept {
	return !std::memcmp( data() + ( length() - s.length() ), s.data(), s.length() );
}

bool SRString::ends_with( std::string_view s ) const noexcept {
	return !std::memcmp( data() + ( length() - s.length() ), s.data(), s.length() );
}

bool SRString::ends_with( const char *s ) const noexcept {
	auto len = std::strlen( s );
	return !std::memcmp( data() + ( length() - len ), s, len );
}

bool SRString::ends_with( char ch ) const noexcept {
	return length() && back() == ch;
}

SRString &SRString::replace( size_t pos, size_t count, std::string_view str ) noexcept {
	if ( pos >= length() ) return *this;
	if ( str.length() > count && length() + str.length() > capacity() )
		if ( !reserve( length() + str.length() ) ) return *this;
	if ( str.length() > count && shift_right( pos + count, str.length() - count ) ) {
		std::memcpy( _data() + pos, str.data(), str.length() );
		iLength = std::strlen( data() );
	} else if ( str.length() < count && shift_left( pos, count - str.length() ) ) {
		std::memcpy( _data() + pos, str.data(), str.length() );
		iLength = std::strlen( data() );
	} else
		std::memcpy( _data() + pos, str.data(), str.length() );
	return *this;
}

SRString &SRString::replace( SRString::iterator it, size_t count, std::string_view str ) noexcept {
	return replace( &( *it ) - data(), count, str );
}

SRString &SRString::replace( SRString::iterator from, SRString::iterator to, std::string_view str ) noexcept {
	if ( &( *to ) > &( *from ) ) std::swap( from, to );
	return replace( &( *from ) - data(), &( *to ) - &( *from ), str );
}

SRString &SRString::replace( size_t pos, SRString::iterator to, std::string_view str ) noexcept {
	size_t end = &( *to ) - data();
	if ( pos > end ) return *this;
	return replace( pos, end - pos, str );
}

SRString &SRString::replace_first( std::string_view old, std::string_view str ) noexcept {
	return replace( find( old ), old.length(), str );
}

SRString &SRString::replace_all( std::string_view old, std::string_view str ) noexcept {
	while ( true ) {
		auto pos = find( old );
		if ( pos == static_cast<size_t>( -1 ) ) break;
		replace( pos, old.length(), str );
	}
	return *this;
}

SRString SRString::substr( size_t pos, size_t count ) const noexcept {
	if ( pos >= length() ) return SRString();
	if ( pos + count >= length() ) count -= ( pos + count ) - length();
	return SRString( data() + pos, count );
}

SRString SRString::substr( SRString::iterator it, size_t count ) const noexcept {
	return substr( &( *it ) - data(), count );
}

SRString SRString::substr( SRString::iterator from, SRString::iterator to ) const noexcept {
	if ( &( *to ) > &( *from ) ) std::swap( from, to );
	return substr( &( *from ) - data(), &( *to ) - &( *from ) );
}

size_t SRString::copy( char *dest, size_t count, size_t pos ) const noexcept {
	if ( pos >= length() ) return 0;
	if ( pos + count >= length() ) count -= ( pos + count ) - length();
	std::memcpy( dest, data() + pos, count );
	return count;
}

bool SRString::resize( size_t count, char ch ) noexcept {
	if ( count >= capacity() )
		if ( !reserve( count ) ) return false;
	if ( count < length() ) {
		_data()[count] = '\0';
		iLength		   = count;
	} else if ( count > length() ) {
		std::memset( _data() + length(), ch, count - length() );
		_data()[count] = '\0';
		iLength		   = count;
	}
	return true;
}

void SRString::swap( SRString &other ) noexcept {
	std::swap( string, other.string );
	std::swap( iAllocated, other.iAllocated );
	std::swap( iLength, other.iLength );
}

SRString &SRString::to_upper() {
	std::transform( begin(), end(), begin(), ::toupper );
	return *this;
}

SRString &SRString::to_lower() {
	std::transform( begin(), end(), begin(), ::tolower );
	return *this;
}

SRString SRString::as_upper() {
	return as_upper( *this );
}

SRString SRString::as_lower() {
	return as_lower( *this );
}

std::deque<SRString> SRString::split( char delim ) {
	std::stringstream ss( data() );
	std::string		  item;
	item.reserve( capacity() );
	std::deque<SRString> splittedStrings;
	while ( std::getline( ss, item, delim ) ) splittedStrings.push_back( item.data() );
	return splittedStrings;
}

void SRString::join( const std::deque<SRString> &arr ) noexcept {
	for ( auto &&str : arr ) append( str );
}

void SRString::operator+=( const std::deque<SRString> &arr ) noexcept {
	join( arr );
}

void SRString::operator=( const std::deque<SRString> &arr ) noexcept {
	clear();
	join( arr );
}

#if defined( WIN32 ) || defined( _WIN32 )
#	if __cplusplus > 201703L or defined( _MSC_VER )
SRString SRString::u8_to_cp1251( const char8_t *str ) {
#	else
SRString SRString::u8_to_cp1251( const char *str ) {
#	endif
	SRString res;
	int		 result_u, result_c;


	result_u = MultiByteToWideChar( CP_UTF8, 0, (const char *)str, -1, 0, 0 );

	if ( !result_u ) return 0;

	wchar_t *ures = new wchar_t[result_u];

	if ( !MultiByteToWideChar( CP_UTF8, 0, (const char *)str, -1, ures, result_u ) ) {
		delete[] ures;
		return 0;
	}


	result_c = WideCharToMultiByte( 1251, 0, ures, -1, 0, 0, 0, 0 );

	if ( !result_c ) {
		delete[] ures;
		return 0;
	}

	res.resize( result_c );

	if ( !WideCharToMultiByte( 1251, 0, ures, -1, (char *)res.data(), result_c, 0, 0 ) ) {
		delete[] ures;
		return 0;
	}
	delete[] ures;
	return res;
}

#	if __cplusplus > 201703L or defined( _MSC_VER )
SRString SRString::cp1251_to_u8( const char8_t *str ) {
#	else
SRString SRString::cp1251_to_u8( const char *str ) {
#	endif
	SRString res;
	int		 result_u, result_c;


	result_u = MultiByteToWideChar( 1251, 0, (const char *)str, -1, 0, 0 );

	if ( !result_u ) return 0;

	wchar_t *ures = new wchar_t[result_u];

	if ( !MultiByteToWideChar( 1251, 0, (const char *)str, -1, ures, result_u ) ) {
		delete[] ures;
		return 0;
	}


	result_c = WideCharToMultiByte( CP_UTF8, 0, ures, -1, 0, 0, 0, 0 );

	if ( !result_c ) {
		delete[] ures;
		return 0;
	}

	res.resize( result_c );

	if ( !WideCharToMultiByte( CP_UTF8, 0, ures, -1, (char *)res.data(), result_c, 0, 0 ) ) {
		delete[] ures;
		return 0;
	}
	delete[] ures;
	return res;
}

size_t SRString::u8_length() const noexcept {
	int	 len = 0;
	auto s	 = data();
	while ( *s ) len += ( *s++ & 0xc0 ) != 0x80;
	return len;
}

SRString &SRString::to_cp1251() {
	return assign( as_cp1251() );
}

SRString &SRString::to_u8() {
	return assign( as_u8() );
}

SRString SRString::as_cp1251() {
#	if __cplusplus > 201703L or defined( _MSC_VER )
	return u8_to_cp1251( (const char8_t *)data() );
#	else
	return u8_to_cp1251( data() );
#	endif
}

SRString SRString::as_u8() {
#	if __cplusplus > 201703L or defined( _MSC_VER )
	return cp1251_to_u8( (const char8_t *)data() );
#	else
	return cp1251_to_u8( data() );
#	endif
}
#endif

SRString SRString::as_upper( SRString str ) {
	std::transform( str.begin(), str.end(), str.begin(), ::toupper );
	return str;
}

SRString SRString::as_upper( std::string str ) {
	std::transform( str.begin(), str.end(), str.begin(), ::toupper );
	return SRString( str );
}

SRString SRString::as_upper( std::string_view str ) {
	SRString str_( str );
	std::transform( str_.begin(), str_.end(), str_.begin(), ::toupper );
	return str_;
}

SRString SRString::as_upper( const char *str ) {
	SRString str_( str );
	std::transform( str_.begin(), str_.end(), str_.begin(), ::toupper );
	return str_;
}

SRString SRString::as_lower( SRString str ) {
	std::transform( str.begin(), str.end(), str.begin(), ::tolower );
	return str;
}

SRString SRString::as_lower( std::string str ) {
	std::transform( str.begin(), str.end(), str.begin(), ::tolower );
	return SRString( str );
}

SRString SRString::as_lower( std::string_view str ) {
	SRString str_( str );
	std::transform( str_.begin(), str_.end(), str_.begin(), ::tolower );
	return str_;
}

SRString SRString::as_lower( const char *str ) {
	SRString str_( str );
	std::transform( str_.begin(), str_.end(), str_.begin(), ::tolower );
	return str_;
}

size_t SRString::find( const SRString &str, size_t pos, size_t len ) const noexcept {
	return find( str.data(), pos, len );
}

size_t SRString::find( const std::string &str, size_t pos, size_t len ) const noexcept {
	return find( str.data(), pos, len );
}

size_t SRString::find( std::string_view str, size_t pos, size_t len ) const noexcept {
	return find( str.data(), pos, len );
}

size_t SRString::find( const char *str, size_t pos, size_t len ) const noexcept {
	// Modify this string when searching
	if ( pos >= length() ) return static_cast<size_t>( -1 );
	if ( len == static_cast<size_t>( -1 ) ) len = length() - pos;
	auto bak					  = data()[pos + len];
	( (char *)data() )[pos + len] = '\0';
	auto result					  = std::strstr( data() + pos, str );
	( (char *)data() )[pos + len] = bak;
	if ( !result ) return static_cast<size_t>( -1 );
	return result - data();
}

size_t SRString::find( char ch, size_t pos ) const noexcept {
	if ( pos >= length() ) return static_cast<size_t>( -1 );
	for ( size_t i = pos; i < length(); ++i )
		if ( data()[i] == ch ) return i;
	return static_cast<size_t>( -1 );
}

size_t SRString::find( const SRString &str, SRString::iterator it, size_t len ) const noexcept {
	return find( str, &( *it ) - data(), len );
}

size_t SRString::find( const std::string &str, SRString::iterator it, size_t len ) const noexcept {
	return find( str, &( *it ) - data(), len );
}

size_t SRString::find( std::string_view str, SRString::iterator it, size_t len ) const noexcept {
	return find( str, &( *it ) - data(), len );
}

size_t SRString::find( const char *str, SRString::iterator it, size_t len ) const noexcept {
	return find( str, &( *it ) - data(), len );
}

size_t SRString::find( char ch, SRString::iterator it ) const noexcept {
	return find( ch, &( *it ) - data() );
}

size_t SRString::find( const SRString &str, SRString::iterator from, SRString::iterator to ) const noexcept {
	if ( &( *to ) > &( *from ) ) std::swap( from, to );
	return find( str, &( *from ) - data(), &( *to ) - &( *from ) );
}

size_t SRString::find( const std::string &str, SRString::iterator from, SRString::iterator to ) const noexcept {
	if ( &( *to ) > &( *from ) ) std::swap( from, to );
	return find( str, &( *from ) - data(), &( *to ) - &( *from ) );
}

size_t SRString::find( std::string_view str, SRString::iterator from, SRString::iterator to ) const noexcept {
	if ( &( *to ) > &( *from ) ) std::swap( from, to );
	return find( str, &( *from ) - data(), &( *to ) - &( *from ) );
}

size_t SRString::find( const char *str, SRString::iterator from, SRString::iterator to ) const noexcept {
	if ( &( *to ) > &( *from ) ) std::swap( from, to );
	return find( str, &( *from ) - data(), &( *to ) - &( *from ) );
}

size_t SRString::rfind( const SRString &str, size_t pos, size_t len ) const noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = str.length();
	return rfind( str.data(), pos, len );
}

size_t SRString::rfind( const std::string &str, size_t pos, size_t len ) const noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = str.length();
	return rfind( str.data(), pos, len );
}

size_t SRString::rfind( std::string_view str, size_t pos, size_t len ) const noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = str.length();
	return rfind( str.data(), pos, len );
}

size_t SRString::rfind( const char *str, size_t pos, size_t len ) const noexcept {
	if ( len == static_cast<size_t>( -1 ) ) len = std::strlen( str );
	if ( pos == static_cast<size_t>( -1 ) ) pos = length() - len;
	if ( !pos || pos >= length() ) return static_cast<size_t>( -1 );
	for ( size_t i = pos; i != static_cast<size_t>( -1 ); --i )
		if ( !std::memcmp( data() + i, str, len ) ) return i;
	return static_cast<size_t>( -1 );
}

size_t SRString::rfind( char ch, size_t pos ) const noexcept {
	if ( pos == static_cast<size_t>( -1 ) ) pos = length() - 1;
	if ( pos >= length() ) return static_cast<size_t>( -1 );
	for ( size_t i = pos; i != static_cast<size_t>( -1 ); --i )
		if ( data()[i] == ch ) return i;
	return static_cast<size_t>( -1 );
}

size_t SRString::rfind( const SRString &str, SRString::iterator it, size_t len ) const noexcept {
	return rfind( str, &( *it ) - data(), len );
}

size_t SRString::rfind( const std::string &str, SRString::iterator it, size_t len ) const noexcept {
	return rfind( str, &( *it ) - data(), len );
}

size_t SRString::rfind( std::string_view str, SRString::iterator it, size_t len ) const noexcept {
	return rfind( str, &( *it ) - data(), len );
}

size_t SRString::rfind( const char *str, SRString::iterator it, size_t len ) const noexcept {
	return rfind( str, &( *it ) - data(), len );
}

size_t SRString::rfind( char ch, SRString::iterator it ) const noexcept {
	return rfind( ch, &( *it ) - data() );
}

char *SRString::_data() noexcept {
	return (char *)data();
}

bool SRString::shift_right( size_t from, size_t count ) noexcept {
	if ( count < 1 ) return false;
	if ( length() + count > capacity() )
		if ( !reserve( length() + count ) ) return false;
	for ( size_t i = ( length() + count ) - 1; i >= from; --i ) _data()[i] = _data()[i - count];
	return true;
}

bool SRString::shift_left( size_t from, size_t count ) noexcept {
	if ( from >= length() ) return false;
	if ( from + count >= capacity() ) count -= ( from + count ) - capacity();
	for ( size_t i = from; i < length() + count; ++i ) _data()[i] = _data()[i + count];
	return true;
}

SRString operator+( const SRString &lhs, const SRString &rhs ) noexcept {
	SRString result( lhs );
	result.append( rhs );
	return result;
}

SRString operator+( const SRString &lhs, const std::string &rhs ) noexcept {
	SRString result( lhs );
	result.append( rhs );
	return result;
}

SRString operator+( const SRString &lhs, std::string_view rhs ) noexcept {
	SRString result( lhs );
	result.append( rhs );
	return result;
}

SRString operator+( const SRString &lhs, const char *rhs ) noexcept {
	SRString result( lhs );
	result.append( rhs );
	return result;
}

SRString operator+( const SRString &lhs, char rhs ) noexcept {
	SRString result( lhs );
	result.append( rhs );
	return result;
}

bool operator==( const SRString &lhs, const SRString &rhs ) noexcept {
	return lhs.compare( rhs ) == 0;
}

bool operator==( const SRString &lhs, const std::string &rhs ) noexcept {
	return lhs.compare( rhs ) == 0;
}

bool operator==( const SRString &lhs, std::string_view rhs ) noexcept {
	return lhs.compare( rhs ) == 0;
}

bool operator==( const SRString &lhs, const char *rhs ) noexcept {
	return lhs.compare( rhs ) == 0;
}

bool operator==( const SRString &lhs, char rhs ) noexcept {
	return lhs.compare( rhs ) == 0;
}

bool operator!=( const SRString &lhs, const SRString &rhs ) noexcept {
	return lhs.compare( rhs ) != 0;
}

bool operator!=( const SRString &lhs, const std::string &rhs ) noexcept {
	return lhs.compare( rhs ) != 0;
}

bool operator!=( const SRString &lhs, std::string_view rhs ) noexcept {
	return lhs.compare( rhs ) != 0;
}

bool operator!=( const SRString &lhs, const char *rhs ) noexcept {
	return lhs.compare( rhs ) != 0;
}

bool operator!=( const SRString &lhs, char rhs ) noexcept {
	return lhs.compare( rhs ) != 0;
}

std::basic_ostream<char> &operator<<( std::basic_ostream<char> &os, const SRString &str ) {
	return os << str.data();
}

std::basic_istream<char> &operator>>( std::basic_istream<char> &is, SRString &str ) {
	return is.get( (char *)str.data(), str.capacity() );
}
